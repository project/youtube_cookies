## YouTube Cookies

This module attempts to make the YouTube videos played on the site
with the cookie privacy policy. If the user has not enabled the
YouTube-related cookies, the YouTube videos will be displayed with a façade.

### Compatibility
- OneTrust
- EU Cookie Compliance (https://www.drupal.org/project/eu_cookie_compliance)

### Integrations
- [Iframe](https://www.drupal.org/project/iframe) Field type.
- CKEditor: Filter to prevent loading YouTube iframes.

### Configuration
1. Go to /admin/config/system/youtube-cookies
2. Fill the 'Cookie category' with the category machine name that should be accepted to play the YouTube videos.
3. Select your 'Cookie compliance system'.
4. Set the 'Action when the user have not consent'. The youtube-nocookie.com domain option is deprecated as it is **not fully GDPR compliant** and will be removed in future versions.
5. Customise the pop-up message and save it.

To block YouTube videos added via CKEditor, go to the text format and enable the "YouTube cookies filter".
