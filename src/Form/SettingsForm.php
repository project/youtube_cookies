<?php

namespace Drupal\youtube_cookies\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure youtube_cookies settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_cookies_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['youtube_cookies.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('youtube_cookies.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable YouTube cookies'),
      '#description' => $this->t('Uncheck this box to temporarily disable YouTube cookies functionality.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['cookie_category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie category'),
      '#description' => $this->t('The machine name of the cookie compliance category that must be accepted to play youtube videos.'),
      '#default_value' => $config->get('cookie_category'),
      '#required' => TRUE,
    ];

    $form['provider'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Cookie compliance system'),
      '#description' => $this->t('The system that provides the cookie compliance features. Examples: onetrust, EU cookie compliance.'),
      '#default_value' => $config->get('provider'),
      '#options' => [
        'onetrust' => $this->t('One trust'),
        'eu_cookie_compliance' => $this->t('Eu cookie compliance'),
      ],
    ];

    $form['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action when the user have not consent.'),
      '#options' => [
        'popup' => $this->t('Show a popup to the user to accept targeting cookies when clicking the play button (GDPR compliant).'),
        'no_cookies_domain' => $this->t('Use youtube-nocookie.com domain. (Not recommended - Deprecated)'),
      ],
      '#description' => $this->t('The popup will block the user to play the video until accept targeting cookies. The youtube cookies domain delays cookies until the video is played.'),
      '#default_value' => $config->get('action'),
    ];

    $form['popup_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Popup message'),
      '#default_value' => $config->get('popup_message'),
    ];

    $form['button_manage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for the Manage button'),
      '#default_value' => $config->get('button_manage'),
      '#required' => TRUE,
    ];

    $form['button_accept'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for the Accept button'),
      '#default_value' => $config->get('button_accept'),
      '#required' => TRUE,
    ];

    $form['button_exit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for the Exit button'),
      '#default_value' => $config->get('button_exit'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('youtube_cookies.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('cookie_category', $form_state->getValue('cookie_category'))
      ->set('provider', $form_state->getValue('provider'))
      ->set('action', $form_state->getValue('action'))
      ->set('popup_message', $form_state->getValue('popup_message'))
      ->set('button_manage', $form_state->getValue('button_manage'))
      ->set('button_accept', $form_state->getValue('button_accept'))
      ->set('button_exit', $form_state->getValue('button_exit'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
