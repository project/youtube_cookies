<?php

namespace Drupal\youtube_cookies\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a 'youtube_cookies_filter' filter.
 *
 * @Filter(
 *   id = "youtube_cookies_wysiwyg_filter",
 *   title = @Translation("Youtube cookies filter"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {},
 *    description = @Translation("Prevents youtube iframe from loading and adding cookies until consent is given."),
 *   weight = 100
 * )
 */
class YoutubeCookiesWysiwygFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    $config = \Drupal::config('youtube_cookies.settings');
    if (!$config->get('enabled')) {
      return $result;
    }

    if (stristr($text, '<iframe') === FALSE) {
      return $result;
    }

    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);

    foreach ($xpath->query('//iframe[contains(@src,"youtube.com") or contains(@src,"youtu.be")]') as $node) {
      /** @var \DOMElement $node */
      $src = $node->getAttribute('src');
      $node->setAttribute('data-src', $src);
      $node->setAttribute('src', '');

      $current_classes = $node->hasAttribute('class') ? $node->getAttribute('class') : '';

      $node->setAttribute('class', $current_classes . ' youtube-cookies__iframe youtube-cookies__iframe--wysiwyg');
    }

    $result->setProcessedText(Html::serialize($dom));

    return $result;
  }

}
