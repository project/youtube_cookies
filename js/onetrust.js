/**
 * @file
 * Do not let videos be played until accept Onetrust cookies.
 */

(function (Drupal) {

  'use strict';

  Drupal.youtubeCookiesOneTrust = {};

  /**
   * Informs whether youtube specific cookies are accepted or not.
   *
   * @returns {boolean}
   *   True when youtube cookies are accepted.
   */
  Drupal.youtubeCookiesOneTrust.oneTrustCookiesAccepted = function() {
    return typeof(OnetrustActiveGroups) !== 'undefined' && OnetrustActiveGroups.indexOf(drupalSettings.youtubeCookies.cookieCategory) !== -1;
  };

  /**
   * Setup youtube cookies integration for onetrust.
   */
  Drupal.youtubeCookiesOneTrust.setupOnetrust = function() {
    Drupal.youtubeCookies.setAcceptedCookies(Drupal.youtubeCookiesOneTrust.oneTrustCookiesAccepted());
    Drupal.youtubeCookies.setAcceptedCookiesCallback(Drupal.youtubeCookiesOneTrust.oneTrustCookiesAccepted);
  };

  Drupal.youtubeCookiesOneTrust.manageCookiesCallback = function() {
    if (typeof(OneTrust) === 'object') {
      OneTrust.ToggleInfoDisplay();
    }
  };

  Drupal.youtubeCookiesOneTrust.onConsentChange = function() {
    if (typeof(OneTrust) === 'object') {
      OneTrust.OnConsentChanged(function (){
        Drupal.youtubeCookies.setAcceptedCookies(Drupal.youtubeCookiesOneTrust.oneTrustCookiesAccepted())
      });
    }
  };

  /**
   * Behavior description.
   */
  Drupal.behaviors.youtubeCookiesOneTrust = {
    attach: function (context, settings) {

      Drupal.youtubeCookiesOneTrust.setupOnetrust();
      Drupal.youtubeCookies.setShowThirdPartyButton(false);
      Drupal.youtubeCookies.setManageCookiesCallback(Drupal.youtubeCookiesOneTrust.manageCookiesCallback);

      /**
       * Setup youtube videos based in onetrust current values /  events.
       *
       * This code should run just once after the OneTrust cookies are accepted.
       * The reason an interval is used is because OneTrust variable is asynchronously
       * loaded and this is the way to notice when onetrust is loaded,
       * and no documentation of onetrust load events have been found for now
       * (which would prevent this workaround).
       *
       * @type {NodeJS.Timeout}
       */
      let youtubeOnetrustCookiesInterval = setInterval(function() {
        if (typeof(OneTrust) === 'undefined' || typeof(OneTrust.OnConsentChanged) === 'undefined') {
          return;
        }

        Drupal.youtubeCookies.setShowThirdPartyButton(true);
        Drupal.youtubeCookiesOneTrust.setupOnetrust();
        Drupal.youtubeCookiesOneTrust.onConsentChange();
        clearInterval(youtubeOnetrustCookiesInterval);

      }, 100);
    }
  };

} (Drupal));
