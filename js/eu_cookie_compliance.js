/**
 * @file
 * Do not let videos be played until accept EUCC cookies.
 */
(function(Drupal, drupalSettings) {
  Drupal.youtubeCookiesEuCookieCompliance = {};

  /**
   * Informs whether YouTube specific cookies are accepted or not.
   *
   * @return {boolean}
   *   True when YouTube cookies are accepted.
   */
  Drupal.youtubeCookiesEuCookieCompliance.euCookieComplianceCookiesAccepted = () => {
    return Drupal.eu_cookie_compliance.hasAgreedWithCategory(
      drupalSettings.youtubeCookies.cookieCategory
    );
  };

  /**
   * Setup YouTube cookies integration for EU cookie compliance.
   */
  Drupal.youtubeCookiesEuCookieCompliance.setupEuCookieCompliance = () => {
    Drupal.youtubeCookies.setAcceptedCookies(
      Drupal.youtubeCookiesEuCookieCompliance.euCookieComplianceCookiesAccepted()
    );
  };

  Drupal.youtubeCookiesEuCookieCompliance.manageCookiesCallback = () => {
    Drupal.youtubeCookies.removePopup();

    // There is currently no action in the EUCC module to reconfigure the
    // categories. This is a workaround to not lose the categories currently
    // selected by the user.
    // Another option is to re-accept the cookies with the Withdraw action.
    // Drupal.eu_cookie_compliance.withdrawAction();
    Drupal.eu_cookie_compliance.setStatus(null);
    Drupal.eu_cookie_compliance.execute();
  };

  /**
   * Behavior description.
   */
  Drupal.behaviors.youtubeCookiesEuCookieCompliance = {
    attach(context, settings) {
      if (Drupal.eu_cookie_compliance) {
        Drupal.youtubeCookies.setManageCookiesCallback(
          Drupal.youtubeCookiesEuCookieCompliance.manageCookiesCallback
        );

        const postPreferencesLoadHandler = () => {
          Drupal.youtubeCookiesEuCookieCompliance.setupEuCookieCompliance();
        };
        Drupal.eu_cookie_compliance(
          "postPreferencesLoad",
          postPreferencesLoadHandler
        );

        const postPreferencesSaveHandler = () => {
          Drupal.youtubeCookiesEuCookieCompliance.setupEuCookieCompliance();
        };
        Drupal.eu_cookie_compliance(
          "postPreferencesSave",
          postPreferencesSaveHandler
        );
      }
    }
  };
})(Drupal, drupalSettings);
